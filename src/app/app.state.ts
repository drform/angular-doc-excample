import { User } from './Models/user.model';

export interface AppState {
  readonly user: User;
}
