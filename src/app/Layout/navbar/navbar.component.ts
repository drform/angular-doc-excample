import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';
import { AppState } from '../../app.state';
import * as UserActions from '../../actions/user.actions';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    private routes: Router,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
  }

  logout() {
    this.store.dispatch(new UserActions.RemoveUser());
    this.routes.navigate(['/']);
  }

}
