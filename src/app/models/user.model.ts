export interface User {
  user_info?: UserInfo;
  token?: string;
  error?: string;
}

export interface UserInfo {
  id: number;
  username: string;
  name: string;
  email: string;
  tel: string;
  position: string;
  created_at: Date;
  updated_at: Date;
  role: string;
  permission: string;
  image: any[];
}
