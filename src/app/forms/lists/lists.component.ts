import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../Services/auth.service';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css']
})
export class ListsComponent implements OnInit {

  forms: any = [];

  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.getForm();
  }

  getForm() {
    this.auth.getForm().subscribe(data => {
      this.forms = data;
    });
  }

}
