import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowComponent } from '../show/show.component';
import { DetailComponent } from './detail/detail.component';
import { AnswerComponent } from './answer/answer.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: ShowComponent },
    ]
  },
  { path: 'answer', component: AnswerComponent },
  { path: 'detail/:id', component: DetailComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShowRoutingModule { }
