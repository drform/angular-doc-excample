import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShowRoutingModule } from './show-routing.module';
import { ShowComponent } from '../show/show.component';
import { DetailComponent } from './detail/detail.component';
import { AnswerComponent } from './answer/answer.component';

@NgModule({

  imports: [
    CommonModule,
    ShowRoutingModule
  ],
  declarations: [
    ShowComponent,
    DetailComponent,
    AnswerComponent
  ],
})
export class ShowModule { }
