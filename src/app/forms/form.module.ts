import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from '../Layout/layout.module';

import { FormRoutingModule } from './form-routing.module';
import { FormComponent } from './form.component';
import { ListsComponent } from './lists/lists.component';
import { NewComponent } from './new/new.component';
import { EditComponent } from './edit/edit.component';
import { SharesComponent } from './shares/shares.component';


@NgModule({
  imports: [
    CommonModule,
    FormRoutingModule,
    FormsModule,
    LayoutModule
  ],
  declarations: [
    FormComponent,
    ListsComponent,
    NewComponent,
    EditComponent,
    SharesComponent,

  ],
})
export class FormModule { }
