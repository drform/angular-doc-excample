import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from './form.component';
import { ListsComponent } from './lists/lists.component';
import { NewComponent } from './new/new.component';
import { EditComponent } from './edit/edit.component';
import { SharesComponent } from './shares/shares.component';

const routes: Routes = [
  {
    path: '',
    component: FormComponent,
    children: [
      { path: '', component: ListsComponent },
      { path: 'share', component: SharesComponent },
      { path: 'new', component: NewComponent },
      { path: 'edit', component: EditComponent },
      {
        path: 'show/:id',
        loadChildren: 'src/app/forms/show/show.module#ShowModule',
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormRoutingModule { }
