import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Host } from '../../environments/environment';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  postSignin(data) {
    const params = { session: data };
    return this.http.post(HOST_URL + '/login', params);
  }

  getForm() {
    return this.http.get(HOST_URL + '/forms');
  }

}
