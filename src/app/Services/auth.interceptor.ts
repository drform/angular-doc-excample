import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

import { User } from './../Models/user.model';
import { Store } from '@ngrx/store';
import { AppState } from './../app.state';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  user: User;

  constructor(
    public auth: AuthService,
    private store: Store<AppState>,
    private routes: Router
  ) {
    store.select('user').subscribe(data => {
      this.user = data;
    });
  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (this.user) {
      request = request.clone({
        setHeaders: {
          'token': this.user.token
        },
      });
    }
    return next.handle(request);
  }
}
