import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../Layout/layout.module';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import { UserComponent } from './user/user.component';

@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    LayoutModule
  ],
  declarations: [
    SettingsComponent,
    UserComponent
  ]
})
export class SettingsModule { }
