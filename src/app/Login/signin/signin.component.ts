import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../Services/auth.service';
import { LoginModel } from '../../Models/login.model';
import { User } from '../../Models/user.model';

import { Store } from '@ngrx/store';
import { AppState } from '../../app.state';
import * as UserActions from '../../actions/user.actions';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  login: LoginModel = {
    username: 'ggez123',
    password: '12345678'
  };
  user: User;

  constructor(
    private routes: Router,
    private authService: AuthService,
    private store: Store<AppState>
  ) {
  }

  ngOnInit() {
  }

  signIn() {
    this.authService.postSignin(this.login).subscribe(data => {
      this.user = data;
      if (!this.user.error) {
        this.store.dispatch(new UserActions.AddUser(this.user));
        this.routes.navigate(['/forms']);
      } else {
        this.login.username = '';
        this.login.password = '';
      }
    });

  }

}
